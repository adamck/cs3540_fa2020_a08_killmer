_Assignment 4 - 7:_
 
**What went well?** All of the methods we used for our projects were fairly easy to use and implement.

**What didn't go so well?** It took us a  bit to understand CI/CD, and setting up a makefile was a little confusing at first, but we quickly got the hang of it.

**What have I learned?** Before this class, I was not familiar with continuous integration or test driven development. Both methods appear to be very useful when working on a project with other programmers, or even on a solo project. Once we got things working, it made the process of getting working code tested and ready to use a lot smoother.

**What still puzzles me?** I feel that I still struggle when trying to come up with tests to test our code with. I understand how they work and their purpose, but I struggled most with actually creating tests. Once I get used to working on projects using tests and create more examples, I feel I will start to become proficient.
 
 
_Day 1 to now:_

**What went well?** Same as above, getting the hang of everything was fairly easy, and it is easy to understand how all of what we learned is useful.

**What didn't go so well?** As well as continuous integration and makefiles, user stories took us a while to get the hang of creating.

**What have I learned?** As well as CI and test driven development, user stories and estimation/prioritization seemed quite useful, and made the process of planning and actually creating a project a lot easier and smoother. 

**What still puzzles me?** I don't feel like I got enough experience with estimation. While I understand how to do it and what it is useful for, I struggle sometimes with trying to weight tasks, and then combining everything together into an estimation of how long the project will take to finish.